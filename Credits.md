# Credits
This file contains all the people who've ever contributed to MRCL.

### Note
**The formatting of this file needs to be fixed and the names need to be updated**

## Builds
### Old Kadic: 
https://www.planetminecraft.com/project/code-lyoko---kadic-academy/

### Refurbished old Kadic
 * alexmario5 
 * Ten

### Forest replica and derivatives
 * Noojin 
 * Ten 

### Carthage
* Noojin 
* Ten

### Ice sector
* Noojin 
* Ten

### New Kadic (base): 
* KaruzoHikari 
* Jack 

### New Kadic (decoration): 
* KaruzoHikari 
* TankadiN 
* Jack 
* Luclyoko 

### New factory:
* TankadiN 
* TheLeech 
* KaruzoHikari 


Scanner transition: 
* Jack 
* Noojin (colors)
* alexmario5 (redstone)
* Ten(redstone)

Scanners:
* KaruzoHikari (Animations, models, code)
* Jack (code)



 
Superscan system (and general SC redstone): 
* Ten
Scanner system: 
* Ten 





Factory door:
* KaruzoHikari 
* alexmario5 
* T-3N

Advancements:
* alexmario5 
* KaruzoHikari 

Project Carthage Plugin: (rttp, devirt, superscan frontend, tower activation front end)
* Jack 
* KaruzoHikari

Frontier:
* alexmario5 

Old devirt system:
* alexmario5 

Transporter:
* Ten 
* alexmario5 
* KaruzoHikari 

Texture pack: 
* Ten 
* alexmario5 
* Noojin 
* franz hopper 

Misc sounds:
* juleic1123 

Server logo(s):
* TankadiN 
* Sky

Translations:
 * French: alexmario5 
 * Spanish: KaruzoHikari 
 * Italian: 3DGamer 
 * Project Carthage Translation code: KaruzoHikari 

a huge thanks to all the Alpha Testers and to Sky for financially backing the project. 

Want your name here? apply for staff today!